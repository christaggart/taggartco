---
title: "New Blog, who dis?"
description: "Placeholder: voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
date: "2017-04-30"
categories:
    - "featured"
    - "blah"
---

After a few years of my blog languishing unused, and having my google ranking unseated, here we are. Giving this a go again with [Hugo](https://gohugo.io/)
