---
title: "Building an Open Source Robot Delivery Platform"
description: "Welcome to the second blog post."
date: "2017-04-30"
categories:
    - "template"
    - "second"
    - "boring"
---

Land-based delivery robots that use existing infrastructure are a pretty exciting development, and yet there are seemingly still very few companies getting into this space. I have a suspicions that if Starship technologies' exploits start to gain widespread legislative approval for use on most sidewalks, we will see explosive growth in this space as major shipping carriers compete in this space, and new entrants attempt to disrupt with their own alternative to UPS/Fedex/Canada Post. Neat.

  * Hardware will become commoditized.

  * Software will become differentiator.

  * Open generally succeeds over closed in the longterm.


  One of my goals for this year is to build my own delivery robot using [ROS](http://ros.org), and some off the shelf parts. 
